import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public form = new FormGroup({
    event: new FormControl(null),
    email: new FormControl(null),
    url: new FormControl(null),
  });

  constructor(private http: HttpClient, private n: NotifierService) {}

  public invite() {
    this.http
      .post(`https://${this.form.value.url?.trim()}`, {
        event: this.form.value.event,
        email: this.form.value.email,
      })
      .subscribe({
        next: (_) => {
          this.n.notify(
            'success',
            `${this.form.value.email} invited to ${this.form.value.event} successfully`
          );
          this.form.reset();
        },
        error: (err) => {
          console.error(err);
          this.n.notify(
            'error',
            'Error inviting member. Try again later, or contact an administrator.'
          );
        },
      });
  }
}
