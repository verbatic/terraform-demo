variable "static_site_bucket" {
  type        = object({ id = string, bucket_regional_domain_name = string })
  description = "The bucket that the static site is deployed to"
}

resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "Access identity for Terraform demo"
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = var.static_site_bucket.bucket_regional_domain_name
    origin_id   = var.static_site_bucket.id

    s3_origin_config {
      # This line is a banger
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Cloudfront distribution for Terraform demo"
  default_root_object = "index.html"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.static_site_bucket.id

    viewer_protocol_policy = "allow-all"

    forwarded_values {
      query_string = true
      headers      = ["Origin"]

      cookies {
        forward = "all"
      }
    }
  }
}
