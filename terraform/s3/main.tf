resource "aws_s3_bucket" "static-site" {
  bucket = "site.verbatic.io"
  acl    = "public-read"
  policy = file("s3/policy.json")

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "aws_s3_bucket_object" "dist" {
  for_each = fileset("../web/dist/web", "*")

  bucket = aws_s3_bucket.static-site.bucket
  key    = each.value
  source = "../web/dist/web/${each.value}"
  content_type = "text/html"
  etag   = filemd5("../web/dist/web/${each.value}")
}

output "static_site_bucket" {
  value = aws_s3_bucket.static-site
  description = "The bucket that the static site is deployed to"
}

resource "aws_s3_bucket" "example-bucket" {
  bucket = "example.buckets.advance.io"
  acl    = "public-read"
  policy = file("policy.json")
}
