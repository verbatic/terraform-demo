terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    bucket = "terraform-state.verbatic.io"
    key    = "state"
    region = "af-south-1"
    dynamodb_table  = "terraform"
  }
}

provider "aws" {}

module "s3" {
  source = "./s3"
}

module "cloudfront" {
  source = "./cloudfront"

  static_site_bucket = module.s3.static_site_bucket
}
